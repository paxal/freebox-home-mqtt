# Internals

## Topics

Topics are in freebox/node_id/enpoint_name.  So one topic represents one pair (node, endpoint_name).

Each endpoint_name will be linked to
 * 0 or 1 SLOT (write) endpoint_id 
 * 0 or 1 SIGNAL (read) endpoint_id

## Nodes and tiles

Tiles are used to expose to homeassistant.

Nodes are used to expose raw data to MQTT, eg freebox/node_id/ep_name.

Tiles links data to nodes.

Each TileData should end up to a discovered object.
