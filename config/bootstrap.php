<?php

use Symfony\Component\Config\FileLocator;
use Symfony\Component\Console\DependencyInjection\AddConsoleCommandPass;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\XmlFileLoader;
use Symfony\Component\EventDispatcher\DependencyInjection\RegisterListenersPass;

require_once __DIR__ . '/../vendor/autoload.php';

$container = new ContainerBuilder();
$container->addCompilerPass(new RegisterListenersPass());
$container->addCompilerPass(new AddConsoleCommandPass());
$loader = new XmlFileLoader($container, new FileLocator(__DIR__ . '/../config/'));
$loader->load('services.xml');

$container->compile();

return $container;
