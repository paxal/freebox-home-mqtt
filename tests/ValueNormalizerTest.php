<?php

declare(strict_types=1);

namespace Paxal\FreeboxHomeMqtt\Tests;

use Paxal\FreeboxHomeMqtt\Freebox\Object\ValueType;
use Paxal\FreeboxHomeMqtt\ValueNormalizer;
use PHPUnit\Framework\TestCase;

final class ValueNormalizerTest extends TestCase
{
    /**
     * @return iterable<array{ValueType,string,mixed}>
     */
    public static function getDataForDenormalize(): iterable
    {
        yield [ValueType::VOID, '', null];

        yield [ValueType::BOOL, 'true', true];
        yield [ValueType::BOOL, '1', true];
        yield [ValueType::BOOL, 'on', true];
        yield [ValueType::BOOL, 'false', false];
        yield [ValueType::BOOL, '0', false];
        yield [ValueType::BOOL, 'off', false];

        yield [ValueType::INT, '10', 10];

        yield [ValueType::STRING, 'string', 'string'];
        yield [ValueType::STRING, 'true', 'true'];
        yield [ValueType::STRING, '1', '1'];

        yield [ValueType::FLOAT, '0', 0.];
        yield [ValueType::FLOAT, '.1', .1];
        yield [ValueType::FLOAT, '0.1', .1];
        yield [ValueType::FLOAT, '10.1', 10.1];
    }

    /**
     * @dataProvider getDataForDenormalize
     */
    public function testDenormalize(ValueType $type, string $value, mixed $expected): void
    {
        self::assertSame(expected: $expected, actual: $this->getValueNormalizer()->denormalize($type, $value));
    }

    private function getValueNormalizer(): ValueNormalizer
    {
        return new ValueNormalizer();
    }
}
