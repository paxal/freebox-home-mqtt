# Freebox Home MQTT Bridge

This project aims to expose Freebox Home API via MQTT.

Steps to proceed are:
* [Installation](#installation) to install the project
* [Configuration](#configuration) to configure MQTT Broker and Freebox Access
* [Run](#run) to run the project once Freebox access has been granted

## Requirements

You need the following:
 * A place to run the project, e.g. one of the following:
   * `docker` with `docker compose` - available on Delta VMs
   * a system with PHP 8.1+
   * a VM on the Freebox Delta itself
 * An MQTT broker such as mosquitto
 * For the installation, a physical access to the Freebox Delta

## Installation

### Locally

This project supports PHP versions from 8.1 to 8.3.
If you cannot install these versions locally, you can still use docker or install the project inside a Virtual Machine.

```bash
bin/install.sh # Install dependencies, eg composer and vendor/
cp config/config_dist.yaml config/config.yaml # Copy config, and edit for your needs
docker compose exec php bin/console app:auth # Authenticate Freebox
```

### Using docker compose

```bash
docker compose build # Build docker image: compile opcache and pcntl
docker compose up -d # Run docker container
docker compose exec php bin/install.sh # Install dependencies, eg composer and vendor/
cp config_dist.yaml config.yaml # Copy config, and edit for your needs
docker compose exec php bin/console app:auth # Authenticate Freebox
```

### Authenticate Freebox

Authentication must be done at home, and cannot be done remotely, because the API call to create a token will fail if done remotely.
* You first need to launch `bin/console app:auth` command
* You should then go to your Freebox and accept the connection ✅
* Once it's done, you need to enable Home access:
  * log into your Freebox admin interface, e.g. http://mafreebox.freebox.fr/
  * go to _Freebox parameters_
  * search _Misc > Access management_
  * then go to _Application_ tab, find the application, and then edit permissions to allow access to Home objects (_Gestion de l'alarme et maison connectée_).

## Configuration

You have to configure the project by creating a config/config.yaml file. You can copy
the [`config/config_dist.yaml`](config/config_dist.yaml) to `config/config.yaml` and edit it:
* mqtt server should be configured, at least the host
* freebox token should be saved here as well.

```yaml
# Sample config/config.yaml file, modify for your need
mqtt:
  host: 192.168.1.1
  # @see \PhpMqtt\Client\ConnectionSettings 
  # https://github.com/php-mqtt/client/blob/v2.0.0/src/ConnectionSettings.php
  connection_settings:
    username: 'username'
    password: 'password'

freebox:
  app_token: 'YOUR_TOKEN'

```

### Remote access

If you want a remote access, first generate the token locally, and then configure the
freebox.endpoint accordingly, e.g.:
```yaml
freebox:
  token: YOUR_TOKEN_HERE
  endpoint: https://YOUR_HOSTNAME.freeboxos.fr:YOUR_PORT/
```

You can find the remote access to your Freebox on the web interface, _Misc > Access management_.

## Run

Launch the `app:run` command to register Freebox MQTT. 
