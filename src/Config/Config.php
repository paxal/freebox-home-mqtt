<?php

declare(strict_types=1);

namespace Paxal\FreeboxHomeMqtt\Config;

use Symfony\Component\Yaml\Yaml;

final class Config
{
    private function __construct(private readonly array $config) {}

    public function getFreeboxConfig(): \Traversable
    {
        $freeboxConfig = $this->config['freebox'] ?? [];
        \assert(\is_array($freeboxConfig));

        return new \ArrayIterator($freeboxConfig);
    }

    public function getMqttConfig(): \Traversable
    {
        $mqttConfig = $this->config['mqtt'] ?? [];
        if (!\is_array($mqttConfig)) {
            throw new \InvalidArgumentException('MQTT Configuration is invalid');
        }

        return new \ArrayIterator($mqttConfig);
    }

    public static function create(?string $filename = null): self
    {
        /** @psalm-suppress RiskyTruthyFalsyComparison */
        $filename ??= getenv('FREEBOX_HOME_MQTT_CONFIG') ?: __DIR__ . '/../../config/config.yaml';
        $config = Yaml::parseFile($filename);
        if (!\is_array($config)) {
            throw new \InvalidArgumentException('Unable to parse configuration file, not an array');
        }

        return new self($config);
    }
}
