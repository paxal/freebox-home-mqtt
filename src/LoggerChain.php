<?php

declare(strict_types=1);

namespace Paxal\FreeboxHomeMqtt;

use Psr\Log\AbstractLogger;
use Psr\Log\LoggerInterface;

final class LoggerChain extends AbstractLogger
{
    public function __construct(
        /** @var LoggerInterface[] */
        private array $loggers = []
    ) {}

    public function log($level, \Stringable|string $message, array $context = []): void
    {
        foreach ($this->loggers as $logger) {
            $logger->log($level, $message, $context);
        }
    }

    public function add(LoggerInterface $logger): void
    {
        $this->loggers[] = $logger;
    }
}
