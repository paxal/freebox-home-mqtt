<?php

declare(strict_types=1);

namespace Paxal\FreeboxHomeMqtt;

use Paxal\FreeboxHomeMqtt\Freebox\Discovery;
use Paxal\FreeboxHomeMqtt\Freebox\FreeboxCreatedEvent;
use Paxal\FreeboxHomeMqtt\Freebox\FreeboxUpdatedEvent;
use Paxal\FreeboxHomeMqtt\Freebox\Object\Tile;
use Paxal\FreeboxHomeMqtt\Topic\FreeboxValueTopicFactory;
use Paxal\FreeboxHomeMqtt\Topic\TopicRepository;
use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

final class LifecycleHandler implements EventSubscriberInterface
{
    public function __construct(
        private readonly TopicRepository $topicRepository,
        private readonly MqttClient $mqttClient,
        private readonly ValueNormalizer $valueNormalizer,
        private readonly FreeboxValueTopicFactory $freeboxValueTopicFactory,
        private readonly LoggerInterface $logger,
        private readonly Discovery $discovery,
    ) {}

    private function getTopic(Tile $tile, string $property): string
    {
        return sprintf('freebox/home/%s/%s', $tile->id, $property);
    }

    public function onCreated(FreeboxCreatedEvent $event): void
    {
        $this->logger->info('Got Freebox creation');

        $tile = $event->tile;
        $this->mqttClient->publish($this->getTopic($tile, 'name'), $tile->label, retain: true);
        $this->mqttClient->publish($this->getTopic($tile, 'adapter'), $tile->node->adapter->label, retain: true);
        foreach ($tile->data as $tileData) {
            $signal = $this->freeboxValueTopicFactory->createSignalTopic($tile, $tileData);
            $this->logger->info('Created topic ' . $signal->name);
            $this->topicRepository->add($signal);

            $slot = $this->freeboxValueTopicFactory->createSlotTopic($tile, $tileData);
            $this->logger->info('Created topic ' . $slot->name);
            $this->topicRepository->add($slot);

            $normalized = $this->valueNormalizer->normalize($tileData->value);
            if (\strlen($normalized) > 0) {
                $this->mqttClient->publish($signal->name, $normalized, retain: true);
            }
            $this->mqttClient->publish($slot->name, 'null', retain: true);
        }

        $configurations = $this->discovery->getTopicPayloadFromTile($tile);
        foreach ($configurations as $configuration) {
            $this->topicRepository->add($configuration);
            $this->mqttClient->publish($configuration->name, $configuration->get(), retain: true);
        }
    }

    public function onUpdated(FreeboxUpdatedEvent $event): void
    {
        $this->logger->info('Got Freebox update');

        $tileData = $event->tileData;
        $topic = $this->topicRepository->getByData($tileData);
        if ($topic === null) {
            return;
        }

        $this->mqttClient->publish($topic->name, $this->valueNormalizer->normalize($tileData->value), retain: true);
    }

    public static function getSubscribedEvents(): array
    {
        return [
            FreeboxCreatedEvent::class => 'onCreated',
            FreeboxUpdatedEvent::class => 'onUpdated',
        ];
    }
}
