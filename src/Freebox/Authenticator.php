<?php

declare(strict_types=1);

namespace Paxal\FreeboxHomeMqtt\Freebox;

use Paxal\FreeboxHomeMqtt\Freebox\Config\FreeboxConfig;
use Psr\Http\Message\ResponseInterface;
use React\EventLoop\LoopInterface;
use React\Http\Browser;
use React\Promise\Deferred;
use React\Promise\PromiseInterface;

final class Authenticator
{
    private const PATH_AUTHORIZE = '/api/v8/login/authorize/';
    private const PATH_CHECK = self::PATH_AUTHORIZE . '%d';
    private const LOCAL_ENDPOINT = 'http://mafreebox.freebox.fr';

    private Browser $browser;

    public function __construct(
        private LoopInterface $loop,
        private FreeboxConfig $freeboxConfig,
        Browser $browser,
        private Deserializer $deserializer,
    ) {
        $this->browser = $browser->withBase(self::LOCAL_ENDPOINT);
    }

    /**
     * @return PromiseInterface<string>
     */
    public function authenticate(callable $write): PromiseInterface
    {
        $write('Starting authentication...');

        /** @psalm-suppress RiskyTruthyFalsyComparison */
        $response = $this->browser->post(self::PATH_AUTHORIZE, ['content-type' => 'application/json'], \Safe\json_encode([
            'app_id' => $this->freeboxConfig->appId,
            'app_name' => $this->freeboxConfig->appId,
            'app_version' => '1.0.0',
            'device_name' => gethostname() ?: 'unknown',
        ]));

        return $response
            ->catch($this->failed(...))
            ->then(function (ResponseInterface $response) use ($write): PromiseInterface {
                $write('Waiting for freebox validation: go to your physical freebox and accept incoming request');
                return $this->wait($response);
            });
    }

    private function failed(\Throwable $e): never
    {
        throw new \RuntimeException('Unable to connect to the freebox: ' . $e->getMessage());
    }

    /**
     * @return PromiseInterface<string>
     */
    private function wait(ResponseInterface $response): PromiseInterface
    {
        $authorizeResponse = $this->deserializer->deserialize($response->getBody(), DTO\Auth\AuthorizeResponse::class);
        $id = $authorizeResponse->trackId;
        $appToken = $authorizeResponse->appToken;

        /** @var Deferred<string> $deferred */
        $deferred = new Deferred();
        $sendToken = static function () use ($deferred, $appToken): void {
            $deferred->resolve($appToken);
        };

        // Track until it's OK
        $this->scheduleCheck($id, $sendToken);

        return $deferred->promise();
    }

    private function scheduleCheck(int $id, callable $sendToken): void
    {
        $this->loop->addTimer(1, fn() => $this->check($id, $sendToken));
    }

    private function check(int $id, callable $sendToken): void
    {
        $this
            ->browser
            ->get(sprintf(self::PATH_CHECK, $id))
            ->then(fn(ResponseInterface $response) => $this->checkResponse($response, $id, $sendToken));
    }

    private function checkResponse(ResponseInterface $response, int $id, callable $sendToken): void
    {
        $authorizeProgressResponse = $this->deserializer->deserialize($response->getBody(), DTO\Auth\AuthorizeProgressResponse::class);

        match ($authorizeProgressResponse->status) {
            'pending' => $this->scheduleCheck($id, $sendToken),
            'granted' => $sendToken(),
            default => throw new \RuntimeException('Authentication failed, status ' . $authorizeProgressResponse->status),
        };
    }
}
