<?php

declare(strict_types=1);

namespace Paxal\FreeboxHomeMqtt\Freebox;

use Paxal\FreeboxHomeMqtt\Freebox\Object\TileData;
use Paxal\FreeboxHomeMqtt\Freebox\ObjectRepository\TileRepository;
use Paxal\FreeboxHomeMqtt\ValueNormalizer;
use Psr\Log\LoggerInterface;
use React\Promise\PromiseInterface;

final class ValueUpdater
{
    private const PATH_ENDPOINT = '/api/v8/home/endpoints/%d/%d';

    public function __construct(
        private readonly ValueNormalizer $valueNormalizer,
        private readonly ApiClient $apiClient,
        private readonly TileRepository $tileRepository,
        private readonly LoggerInterface $logger,
    ) {}

    public function update(TileData $tileData, string $value): PromiseInterface
    {
        $tile = $this->tileRepository->getFromTileData($tileData);
        $value = $this->valueNormalizer->denormalize($tileData->valueType, $value);

        return $this
            ->apiClient
            ->secure(
                method: 'PUT',
                path: sprintf(self::PATH_ENDPOINT, $tile->node->id, $tileData->epId),
                body: ['value' => $value],
            )
            ->then(function () use ($tile, $tileData, $value): void {
                $this->logger->info('Successfully updated tile data', [
                    'node' => $tile->node->id,
                    'endpoint' => $tileData->epId,
                    'name' => $tileData->name,
                    'value' => $value,
                ]);
            })
            ->catch(function () use ($tile, $tileData, $value): void {
                $this->logger->error('Unable to push tile data value to Freebox', [
                    'node' => $tile->node->id,
                    'endpoint' => $tileData->epId,
                    'name' => $tileData->name,
                    'value' => $value,
                ]);
            });
    }
}
