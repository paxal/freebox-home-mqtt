<?php

declare(strict_types=1);

namespace Paxal\FreeboxHomeMqtt\Freebox\Config;

final class FreeboxConfig
{
    private const APP_ID = 'net.paxal.freebox-home-mqtt';
    private const ENDPOINT = 'http://mafreebox.freebox.fr';

    public readonly string $appId;

    public function __construct(
        ?string $appId = null,
        public readonly ?string $appToken = null,
        public readonly string $endpoint = self::ENDPOINT,
    ) {
        $this->appId = $appId ?? self::APP_ID;
    }
}
