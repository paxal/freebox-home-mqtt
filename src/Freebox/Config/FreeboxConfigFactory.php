<?php

declare(strict_types=1);

namespace Paxal\FreeboxHomeMqtt\Freebox\Config;

use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;

final class FreeboxConfigFactory
{
    public function __construct(private readonly DenormalizerInterface $denormalizer) {}

    public function create(\Traversable $config): FreeboxConfig
    {
        /** @var FreeboxConfig */
        return $this
            ->denormalizer
            ->denormalize($config, FreeboxConfig::class);
    }
}
