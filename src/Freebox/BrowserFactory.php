<?php

declare(strict_types=1);

namespace Paxal\FreeboxHomeMqtt\Freebox;

use Paxal\FreeboxHomeMqtt\Freebox\Config\FreeboxConfig;
use Psr\Log\LoggerInterface;
use React\EventLoop\LoopInterface;
use React\Http\Browser;

final class BrowserFactory
{
    public function __construct(
        private readonly LoopInterface $loop,
        private readonly FreeboxConfig $freeboxConfig,
        private readonly LoggerInterface $logger,
    ) {}

    public function create(): Browser
    {
        return (new Browser(loop: $this->loop))
            ->withBase($this->freeboxConfig->endpoint)
            ->withRejectErrorResponse(true);
    }
}
