<?php

declare(strict_types=1);

namespace Paxal\FreeboxHomeMqtt\Freebox\DTO;

final class FreeboxResponse
{
    public function __construct(
        public readonly bool $success,
        public readonly array $response,
    ) {}
}
