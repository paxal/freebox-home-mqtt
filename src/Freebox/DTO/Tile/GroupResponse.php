<?php

declare(strict_types=1);

namespace Paxal\FreeboxHomeMqtt\Freebox\DTO\Tile;

final class GroupResponse
{
    public function __construct(public readonly string $label) {}
}
