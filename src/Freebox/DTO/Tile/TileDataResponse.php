<?php

declare(strict_types=1);

namespace Paxal\FreeboxHomeMqtt\Freebox\DTO\Tile;

final class TileDataResponse
{
    public function __construct(
        public readonly ?int $refresh,
        public readonly string $label,
        public readonly string $name,
        public readonly int $epId,
        public readonly ?int $slotId,
        public readonly ?int $signalId,
        public readonly string $valueType,
        public readonly string|int|float|bool|null $value,
        public readonly array $ui,
    ) {}
}
