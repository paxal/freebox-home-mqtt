<?php

declare(strict_types=1);

namespace Paxal\FreeboxHomeMqtt\Freebox\DTO\Tile;

final class TileResponse
{
    /**
     * @param list<TileDataResponse> $data
     */
    public function __construct(
        public readonly string $label,
        public readonly int $nodeId,
        public readonly string $type,
        public readonly string $action,
        public readonly GroupResponse $group,
        /** @var list<TileDataResponse> */
        public readonly array $data,
    ) {}
}
