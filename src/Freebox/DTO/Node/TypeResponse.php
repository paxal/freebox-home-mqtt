<?php

declare(strict_types=1);

namespace Paxal\FreeboxHomeMqtt\Freebox\DTO\Node;

final class TypeResponse
{
    public function __construct(public readonly string $label) {}
}
