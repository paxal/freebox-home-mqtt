<?php

declare(strict_types=1);

namespace Paxal\FreeboxHomeMqtt\Freebox\DTO\Node;

final class NodeResponse
{
    /**
     * @param EndpointResponse[] $showEndpoints
     */
    public function __construct(
        public readonly int $id,
        public readonly int $adapter,
        public readonly string $category,
        public readonly string $name,
        public readonly string $label,
        /** @var EndpointResponse[] */
        public readonly array $showEndpoints,
        public readonly TypeResponse $type,
    ) {}
}
