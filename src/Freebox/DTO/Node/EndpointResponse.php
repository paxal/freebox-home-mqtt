<?php

declare(strict_types=1);

namespace Paxal\FreeboxHomeMqtt\Freebox\DTO\Node;

final class EndpointResponse
{
    public function __construct(
        public readonly int $id,
        public readonly string $epType,
        public readonly string $label,
        public readonly string $name,
        public readonly string $valueType,
        public readonly int|string|float|bool|null $value,
    ) {}
}
