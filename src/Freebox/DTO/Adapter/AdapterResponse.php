<?php

declare(strict_types=1);

namespace Paxal\FreeboxHomeMqtt\Freebox\DTO\Adapter;

final class AdapterResponse
{
    public function __construct(
        public readonly int $id,
        public readonly string $label,
    ) {}
}
