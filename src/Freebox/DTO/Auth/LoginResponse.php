<?php

declare(strict_types=1);

namespace Paxal\FreeboxHomeMqtt\Freebox\DTO\Auth;

final class LoginResponse
{
    public function __construct(public readonly string $challenge) {}
}
