<?php

declare(strict_types=1);

namespace Paxal\FreeboxHomeMqtt\Freebox\DTO\Auth;

final class AuthorizeResponse
{
    public function __construct(
        public readonly int $trackId,
        public readonly string $appToken,
    ) {}
}
