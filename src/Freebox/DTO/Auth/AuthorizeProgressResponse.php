<?php

declare(strict_types=1);

namespace Paxal\FreeboxHomeMqtt\Freebox\DTO\Auth;

final class AuthorizeProgressResponse
{
    public function __construct(public readonly string $status) {}
}
