<?php

declare(strict_types=1);

namespace Paxal\FreeboxHomeMqtt\Freebox\ObjectFactory;

use Paxal\FreeboxHomeMqtt\Freebox\DTO\Tile\TileDataResponse;
use Paxal\FreeboxHomeMqtt\Freebox\DTO\Tile\TileResponse;
use Paxal\FreeboxHomeMqtt\Freebox\FreeboxCreatedEvent;
use Paxal\FreeboxHomeMqtt\Freebox\Object\Tile;
use Paxal\FreeboxHomeMqtt\Freebox\Object\TileAction;
use Paxal\FreeboxHomeMqtt\Freebox\Object\TileData;
use Paxal\FreeboxHomeMqtt\Freebox\Object\TileType;
use Paxal\FreeboxHomeMqtt\Freebox\Object\ValueType;
use Paxal\FreeboxHomeMqtt\Freebox\ObjectRepository\GroupRepository;
use Paxal\FreeboxHomeMqtt\Freebox\ObjectRepository\NodeRepository;
use Paxal\FreeboxHomeMqtt\Freebox\ObjectRepository\TileRepository;
use Psr\EventDispatcher\EventDispatcherInterface;

final class TileFactory
{
    public function __construct(
        private readonly NodeRepository $nodeRepository,
        private readonly GroupRepository $groupRepository,
        private readonly TileRepository $tileRepository,
        private readonly EventDispatcherInterface $eventDispatcher,
    ) {}

    public function handleResponse(TileResponse $tileResponse): Tile
    {
        $node = $this->nodeRepository->get($tileResponse->nodeId);
        if ($node === null) {
            throw new \LogicException('Unable to find corresponding node');
        }

        $tile = $this->tileRepository->get(Tile::hashResponse($tileResponse));
        if ($tile !== null) {
            return $tile;
        }

        $group = $this->groupRepository->getOrCreate($tileResponse->group->label);

        $tile = new Tile(
            id: Tile::hashResponse($tileResponse),
            label: $tileResponse->label,
            node: $node,
            type: TileType::from($tileResponse->type),
            action: TileAction::from($tileResponse->action),
            group: $group,
            data: $this->createFromDataCollectionResponse($tileResponse->data)
        );

        $this->tileRepository->add($tile);
        $this->eventDispatcher->dispatch(new FreeboxCreatedEvent($tile));

        return $tile;
    }

    /**
     * @param list<TileDataResponse> $data
     *
     * @return list<TileData>
     */
    private function createFromDataCollectionResponse(array $data): array
    {
        return array_map($this->createFromDataResponse(...), $data);
    }

    private function createFromDataResponse(TileDataResponse $tileDataResponse): TileData
    {
        return new TileData(
            refresh: $tileDataResponse->refresh,
            label: $tileDataResponse->label,
            name: $tileDataResponse->name,
            epId: $tileDataResponse->epId,
            slotId: $tileDataResponse->slotId,
            signalId: $tileDataResponse->signalId,
            valueType: ValueType::from($tileDataResponse->valueType),
            value: $tileDataResponse->value,
            ui: $tileDataResponse->ui,
        );
    }
}
