<?php

declare(strict_types=1);

namespace Paxal\FreeboxHomeMqtt\Freebox\ObjectFactory;

use Paxal\FreeboxHomeMqtt\Freebox\DTO\Adapter\AdapterResponse;
use Paxal\FreeboxHomeMqtt\Freebox\Object\Adapter;
use Paxal\FreeboxHomeMqtt\Freebox\ObjectRepository\AdapterRepository;

final class AdapterFactory
{
    public function __construct(private readonly AdapterRepository $adapterRepository) {}

    public function handleResponse(AdapterResponse $adapterResponse): Adapter
    {
        $adapter = $this->adapterRepository->get($adapterResponse->id);
        if ($adapter !== null) {
            return $adapter;
        }

        $adapter = new Adapter(
            id: $adapterResponse->id,
            label: $adapterResponse->label,
        );

        $this->adapterRepository->add($adapter);

        return $adapter;
    }
}
