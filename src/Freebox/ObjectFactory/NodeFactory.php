<?php

declare(strict_types=1);

namespace Paxal\FreeboxHomeMqtt\Freebox\ObjectFactory;

use Paxal\FreeboxHomeMqtt\Freebox\DTO\Node\EndpointResponse;
use Paxal\FreeboxHomeMqtt\Freebox\DTO\Node\NodeResponse;
use Paxal\FreeboxHomeMqtt\Freebox\Object\Endpoint;
use Paxal\FreeboxHomeMqtt\Freebox\Object\EndpointType;
use Paxal\FreeboxHomeMqtt\Freebox\Object\Node;
use Paxal\FreeboxHomeMqtt\Freebox\Object\ValueType;
use Paxal\FreeboxHomeMqtt\Freebox\ObjectRepository\AdapterRepository;
use Paxal\FreeboxHomeMqtt\Freebox\ObjectRepository\NodeRepository;

final class NodeFactory
{
    public function __construct(
        private readonly AdapterRepository $adapterRepository,
        private readonly NodeRepository $nodeRepository,
    ) {}

    public function handleResponse(NodeResponse $nodeResponse): Node
    {
        $node = $this->nodeRepository->get($nodeResponse->id);
        if ($node !== null) {
            return $node;
        }

        $adapter = $this->adapterRepository->get($nodeResponse->adapter);
        if ($adapter === null) {
            throw new \LogicException('Unable to find adapter');
        }

        $node = new Node(
            id: $nodeResponse->id,
            adapter: $adapter,
            category: $nodeResponse->category,
            name: $nodeResponse->name,
            label: $nodeResponse->label,
            endpoints: array_map($this->handleEndpointResponse(...), $nodeResponse->showEndpoints),
            typeLabel: $nodeResponse->type->label,
        );

        $this->nodeRepository->add($node);

        return $node;
    }

    private function handleEndpointResponse(EndpointResponse $endpointResponse): Endpoint
    {
        return new Endpoint(
            id: $endpointResponse->id,
            endpointType: EndpointType::from($endpointResponse->epType),
            label: $endpointResponse->label,
            name: $endpointResponse->name,
            valueType: ValueType::from($endpointResponse->valueType),
            value: $endpointResponse->value
        );
    }
}
