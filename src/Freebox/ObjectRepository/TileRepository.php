<?php

declare(strict_types=1);

namespace Paxal\FreeboxHomeMqtt\Freebox\ObjectRepository;

use Paxal\FreeboxHomeMqtt\Freebox\Object\Tile;
use Paxal\FreeboxHomeMqtt\Freebox\Object\TileData;

/** @template-extends AbstractRepository<string, Tile> */
final class TileRepository extends AbstractRepository
{
    public function __construct()
    {
        parent::__construct(static fn(Tile $tile): string => Tile::hash(nodeId: $tile->node->id, type: $tile->type->value, action: $tile->action->value));
    }

    public function getFromTileData(TileData $tileData): Tile
    {
        foreach ($this->objects as $tile) {
            foreach ($tile->data as $tileDataCandidate) {
                if ($tileData === $tileDataCandidate) {
                    return $tile;
                }
            }
        }

        throw new \LogicException('Unable to find registered tile for data');
    }
}
