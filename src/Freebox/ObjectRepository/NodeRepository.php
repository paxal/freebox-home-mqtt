<?php

declare(strict_types=1);

namespace Paxal\FreeboxHomeMqtt\Freebox\ObjectRepository;

use Paxal\FreeboxHomeMqtt\Freebox\Object\Node;

/** @template-extends AbstractRepository<int, Node> */
final class NodeRepository extends AbstractRepository
{
    public function __construct()
    {
        parent::__construct(static fn(Node $node): int => $node->id);
    }
}
