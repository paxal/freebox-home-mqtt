<?php

declare(strict_types=1);

namespace Paxal\FreeboxHomeMqtt\Freebox\ObjectRepository;

use Paxal\FreeboxHomeMqtt\Freebox\Object\Adapter;

/** @template-extends AbstractRepository<int, Adapter> */
final class AdapterRepository extends AbstractRepository
{
    public function __construct()
    {
        parent::__construct(static fn(Adapter $adapter): int => $adapter->id);
    }
}
