<?php

declare(strict_types=1);

namespace Paxal\FreeboxHomeMqtt\Freebox\ObjectRepository;

use Paxal\FreeboxHomeMqtt\Freebox\Object\Group;

/**
 * @template-extends AbstractRepository<string, Group>
 */
final class GroupRepository extends AbstractRepository
{
    public function __construct()
    {
        parent::__construct(static fn(Group $group): string => $group->label);
    }

    public function getOrCreate(string $groupName): Group
    {
        $group = $this->get($groupName);
        if ($group !== null) {
            return $group;
        }

        $group = new Group($groupName);
        $this->add($group);

        return $group;
    }
}
