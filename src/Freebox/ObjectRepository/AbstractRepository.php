<?php

declare(strict_types=1);

namespace Paxal\FreeboxHomeMqtt\Freebox\ObjectRepository;

use Paxal\FreeboxHomeMqtt\ResetEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * @template TKey of string|int
 * @template T of Object
 */
abstract class AbstractRepository implements EventSubscriberInterface
{
    /** @var array<TKey, T> */
    protected array $objects = [];

    public function __construct(
        /** @var \Closure(T):TKey */
        private readonly \Closure $hash,
    ) {}

    /**
     * @param TKey $id
     *
     * @return T|null
     */
    public function get(int|string $id): ?object
    {
        return $this->objects[$id] ?? null;
    }

    /**
     * @param T $object
     */
    public function add(object $object): void
    {
        $this->objects[($this->hash)($object)] = $object;
    }

    public function onReset(): void
    {
        $this->objects = [];
    }

    public static function getSubscribedEvents(): array
    {
        return [ResetEvent::class => 'onReset'];
    }
}
