<?php

declare(strict_types=1);

namespace Paxal\FreeboxHomeMqtt\Freebox;

use Paxal\FreeboxHomeMqtt\Freebox\Object\TileData;
use Symfony\Contracts\EventDispatcher\Event;

final class FreeboxUpdatedEvent extends Event
{
    public function __construct(
        public readonly TileData $tileData,
    ) {}
}
