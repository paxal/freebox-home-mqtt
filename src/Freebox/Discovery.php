<?php

declare(strict_types=1);

namespace Paxal\FreeboxHomeMqtt\Freebox;

use Paxal\FreeboxHomeMqtt\Freebox\Object\Tile;
use Paxal\FreeboxHomeMqtt\Freebox\Object\TileAction;
use Paxal\FreeboxHomeMqtt\Topic\SlotTopic;
use Paxal\FreeboxHomeMqtt\Topic\StaticTopic;
use Paxal\FreeboxHomeMqtt\Topic\Topic;
use Paxal\FreeboxHomeMqtt\Topic\TopicRepository;
use React\Promise\PromiseInterface;

final class Discovery
{
    public function __construct(private readonly TopicRepository $topicRepository) {}

    /**
     * @param Tile $tile
     *
     * @return Topic[]
     */
    public function getTopicPayloadFromTile(Tile $tile): array
    {
        return match ($tile->action) {
            TileAction::STORE_SLIDER => $this->getCoverSlider($tile),
            TileAction::INTENSITY_PICKER => $this->getLightWithIntensity($tile),
            default => []
        };
    }

    /**
     * @param Tile $tile
     * @return Topic[]
     */
    private function getCoverSlider(Tile $tile): array
    {
        $prefix = $this->common($tile)['~'];
        $stop = fn(): ?PromiseInterface => $this->topicRepository->get("{$prefix}/stop/set")?->__invoke('stop');
        $set = fn(int $value): ?PromiseInterface => $this->topicRepository->get("{$prefix}/position/set")?->__invoke((string) $value);

        // Data : position, stop
        return [new StaticTopic("homeassistant/cover/freebox_node_{$tile->node->id}/{$tile->action->value}/config", \Safe\json_encode([
            'command_topic' => "~/command",
            'position_topic' => "~/position/value",
            'position_template' => '{{100 - (value | int) }}',
            'set_position_topic' => "~/position/set",
            'set_position_template' => '{{100 - (position | int) }}',
        ] + $this->common($tile))), new SlotTopic(
            $this->common($tile)['~'] . '/command',
            static fn(string $payload): PromiseInterface => match (strtolower($payload)) {
                'stop' => $stop(),
                'open' => $set(0),
                'close' => $set(100),
            } ?? throw new \LogicException()
        )];
    }

    /**
     * @return Topic[]
     */
    private function getLightWithIntensity(Tile $tile): array
    {
        // Data : switch_state, luminosity
        return [new StaticTopic("homeassistant/light/freebox_node_{$tile->node->id}/{$tile->action->value}/config", \Safe\json_encode([
            'brightness_command_topic' => "~/luminosity/set",
            'brightness_state_topic' => "~/luminosity/value",
            'command_topic' => "~/switch_state/set",
            'payload_on' => 'true',
            'payload_off' => 'false',
            'state_topic' => "~/switch_state/value",
        ] + $this->common($tile)))];
    }

    /**
     * @return array{~:string}&array<string,mixed>
     */
    private function common(Tile $tile): array
    {
        return [
            '~' => "freebox/home/{$tile->id}",
            'name' => "{$tile->group->label} > {$tile->node->label}",
            'unique_id' => $tile->id,
            'device' => [
                'name' => $tile->node->label,
                'manufacturer' => $tile->node->adapter->label,
                'suggested_area' => $tile->group->label,
                'via_device' => 'Freebox',
                'identifiers' => [
                    "freebox_{$tile->id}",
                ],
                'model' => $tile->node->typeLabel,
            ],
        ];
    }
}
