<?php

declare(strict_types=1);

namespace Paxal\FreeboxHomeMqtt\Freebox;

use Paxal\FreeboxHomeMqtt\Freebox\Config\FreeboxConfig;
use Psr\Http\Message\ResponseInterface;
use Psr\Log\LoggerInterface;
use React\Http\Browser;
use React\Promise\PromiseInterface;

use function React\Promise\resolve;
use function Safe\json_encode as json_encode;

final class Session
{
    private const PATH_CHALLENGE = '/api/v8/login/';
    private const PATH_SESSION = self::PATH_CHALLENGE . 'session/';

    /** @var PromiseInterface<string>|null  */
    private PromiseInterface|null $session = null;
    private bool $renewInProgress = false;

    public function __construct(
        private readonly FreeboxConfig $config,
        private readonly Browser $browser,
        private readonly Deserializer $deserializer,
        private readonly LoggerInterface $logger,
    ) {}

    /**
     * @return PromiseInterface<string>
     */
    public function get(): PromiseInterface
    {
        if ($this->session === null) {
            return $this->create();
        }

        return $this->session;
    }

    /**
     * @return PromiseInterface<string>
     */
    public function renew(): PromiseInterface
    {
        if ($this->renewInProgress) {
            \assert($this->session !== null);

            return $this->session;
        }

        $this->renewInProgress = true;

        $promise = $this->create();

        $promise->then(fn(): bool => $this->renewInProgress = false);

        return $promise;
    }

    /**
     * @return PromiseInterface<string>
     */
    private function create(): PromiseInterface
    {
        return $this
            ->browser
            ->get(self::PATH_CHALLENGE)
            ->then($this->createSession(...))
            ->then($this->saveSession(...));
    }

    /**
     * @return PromiseInterface<ResponseInterface>
     */
    private function createSession(ResponseInterface $response): PromiseInterface
    {
        $loginResponse = $this->deserializer->deserialize($response->getBody(), DTO\Auth\LoginResponse::class);
        $challenge = $loginResponse->challenge;

        if ($this->config->appToken === null) {
            throw new \RuntimeException('No app token found, check your configuration and installation');
        }

        $password = hash_hmac('sha1', $challenge, $this->config->appToken);

        $this->logger->debug('Authenticating');
        return $this->browser->post(self::PATH_SESSION, body: json_encode([
            'app_id' => $this->config->appId,
            'password' => $password,
        ]));
    }

    /**
     * @return PromiseInterface<string>
     */
    private function saveSession(ResponseInterface $response): PromiseInterface
    {
        $sessionResponse = $this->deserializer->deserialize($response->getBody(), DTO\Auth\LoginSessionResponse::class);
        $this->logger->debug('Authenticated successfully');
        $session = $sessionResponse->sessionToken;

        return $this->session = resolve($session);
    }
}
