<?php

declare(strict_types=1);

namespace Paxal\FreeboxHomeMqtt\Freebox;

use Paxal\FreeboxHomeMqtt\Freebox\Object\Tile;
use Symfony\Contracts\EventDispatcher\Event;

final class FreeboxCreatedEvent extends Event
{
    public function __construct(
        public readonly Tile $tile,
    ) {}
}
