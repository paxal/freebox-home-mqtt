<?php

declare(strict_types=1);

namespace Paxal\FreeboxHomeMqtt\Freebox;

use Psr\Http\Message\StreamInterface;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;

use function Safe\json_decode as json_decode;

final class Deserializer
{
    public function __construct(private readonly DenormalizerInterface $denormalizer) {}

    private function fetchResultFromBody(StreamInterface|string $body): array
    {
        $data = json_decode((string) $body, true);
        if (!\is_array($data) || !\array_key_exists(key: 'success', array: $data) || !\is_bool($data['success'])) {
            throw new \RuntimeException('Invalid response from freebox: no success or not bool');
        }

        if (!$data['success']) {
            throw new \RuntimeException('Call failed, success=false');
        }

        if (!\array_key_exists(key: 'result', array: $data)) {
            throw new \RuntimeException('Unable to find result in response');
        }

        $result = $data['result'];
        if (!\is_array($result)) {
            throw new \RuntimeException('Result is not an array');
        }

        return $result;
    }
    /**
     * @template T of object
     *
     * @param class-string<T> $className
     *
     * @return T
     */
    public function deserialize(StreamInterface|string $body, string $className): object
    {
        $result = $this->fetchResultFromBody($body);

        $object = $this->denormalizer->denormalize($result, $className);
        \assert(\is_object($object));
        \assert($object instanceof $className);

        return $object;
    }

    /**
     * @template T of object
     *
     * @param class-string<T> $className
     *
     * @return list<T>
     */
    public function deserializeCollection(StreamInterface|string $body, string $className): array
    {
        $result = $this->fetchResultFromBody($body);

        $objectCollection = $this->denormalizer->denormalize($result, $className . '[]');
        \assert(\is_array($objectCollection));
        \assert(\array_is_list($objectCollection));
        foreach ($objectCollection as $object) {
            \assert(\is_object($object));
            \assert($object instanceof $className);
        }

        /** @var list<T> */
        return $objectCollection;
    }
}
