<?php

declare(strict_types=1);

namespace Paxal\FreeboxHomeMqtt\Freebox\Object;

final class Adapter
{
    public function __construct(
        public readonly int $id,
        public readonly string $label,
    ) {}
}
