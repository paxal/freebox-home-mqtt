<?php

declare(strict_types=1);

namespace Paxal\FreeboxHomeMqtt\Freebox\Object;

enum TileAction: string
{
    case TILESET = 'tileset';
    case GRAPH = 'graph';
    case STORE = 'store';
    case STORE_SLIDER = 'store_slider';
    case COLOR_PICKER = 'color_picker';
    case HEAT_PICKER = 'heat_picker';
    case INTENSITY_PICKER = 'intensity_picker';
    case NONE = 'none';
}
