<?php

declare(strict_types=1);

namespace Paxal\FreeboxHomeMqtt\Freebox\Object;

final class Node
{
    public function __construct(
        public readonly int $id,
        public readonly Adapter $adapter,
        public readonly string $category,
        public readonly string $name,
        public readonly string $label,
        /** @var Endpoint[] */
        public readonly array $endpoints,
        public readonly string $typeLabel,
    ) {}
}
