<?php

declare(strict_types=1);

namespace Paxal\FreeboxHomeMqtt\Freebox\Object;

final class Endpoint
{
    public function __construct(
        public readonly int $id,
        public readonly EndpointType $endpointType,
        public readonly string $label,
        public readonly string $name,
        public readonly ValueType $valueType,
        public int|string|float|bool|null $value,
    ) {}
}
