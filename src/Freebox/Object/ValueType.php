<?php

declare(strict_types=1);

namespace Paxal\FreeboxHomeMqtt\Freebox\Object;

enum ValueType: string
{
    case BOOL = 'bool';
    case STRING = 'string';
    case INT = 'int';
    case FLOAT = 'float';
    case VOID = 'void';
}
