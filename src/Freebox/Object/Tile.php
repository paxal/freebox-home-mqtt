<?php

declare(strict_types=1);

namespace Paxal\FreeboxHomeMqtt\Freebox\Object;

use Paxal\FreeboxHomeMqtt\Freebox\DTO\Tile\TileResponse;

final class Tile
{
    public function __construct(
        public readonly string $id,
        public readonly string $label,
        public readonly Node $node,
        public readonly TileType $type,
        public readonly TileAction $action,
        public readonly Group $group,
        /** @var list<TileData> */
        public readonly array $data,
    ) {}

    public static function hash(int $nodeId, string $type, string $action): string
    {
        return sprintf('%d-%s-%s', $nodeId, $type, $action);
    }

    public static function hashResponse(TileResponse $tileResponse): string
    {
        return self::hash(nodeId: $tileResponse->nodeId, type: $tileResponse->type, action: $tileResponse->action);
    }
}
