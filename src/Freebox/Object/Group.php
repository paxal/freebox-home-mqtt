<?php

declare(strict_types=1);

namespace Paxal\FreeboxHomeMqtt\Freebox\Object;

final class Group
{
    public function __construct(public readonly string $label) {}
}
