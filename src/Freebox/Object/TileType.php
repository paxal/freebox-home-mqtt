<?php

declare(strict_types=1);

namespace Paxal\FreeboxHomeMqtt\Freebox\Object;

enum TileType: string
{
    case ACTION = 'action';
    case INFO = 'info';
    case LIGHT = 'light';
    case ALARM_SENSOR = 'alarm_sensor';
    case CAMERA = 'camera';
}
