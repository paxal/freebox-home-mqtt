<?php

declare(strict_types=1);

namespace Paxal\FreeboxHomeMqtt\Freebox\Object;

final class TileData
{
    public function __construct(
        public readonly ?int $refresh,
        public readonly string $label,
        public readonly string $name,
        public readonly int $epId,
        public readonly ?int $slotId,
        public readonly ?int $signalId,
        public readonly ValueType $valueType,
        public string|int|float|bool|null $value,
        public array $ui,
    ) {}
}
