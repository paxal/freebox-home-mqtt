<?php

declare(strict_types=1);

namespace Paxal\FreeboxHomeMqtt\Freebox\Object;

enum EndpointType: string
{
    /** Actions, pushed to freebox */
    case SLOT = 'slot';

    /** Sensors, read from freebox */
    case SIGNAL = 'signal';
}
