<?php

declare(strict_types=1);

namespace Paxal\FreeboxHomeMqtt\Freebox;

use Psr\Http\Message\ResponseInterface;
use Psr\Log\LoggerInterface;
use React\Http\Browser;
use React\Promise\PromiseInterface;

use function React\Promise\resolve;
use function Safe\json_encode as json_encode;

final class ApiClient
{
    private readonly Browser $browser;

    public function __construct(
        Browser $browser,
        private readonly Session $session,
        private readonly LoggerInterface $logger,
        private readonly Deserializer $deserializer,
    ) {
        $this->browser = $browser->withRejectErrorResponse(false);
    }

    /**
     * @template T of object
     *
     * @param class-string<T> $className
     * @param 'GET'|'PUT'|'POST' $method
     *
     * @return PromiseInterface<T>
     */
    public function secureObject(string $className, string $method, string $path, ?array $body = null): PromiseInterface
    {
        return $this
            ->secure($method, $path, $body)
            ->then(fn(string $serialized): object => $this->deserializer->deserialize($serialized, $className));
    }

    /**
     * @template T of object
     *
     * @param class-string<T> $className
     * @param 'GET'|'PUT'|'POST' $method
     *
     * @return PromiseInterface<list<T>>
     */
    public function secureObjectCollection(string $className, string $method, string $path, ?array $body = null): PromiseInterface
    {
        return $this
            ->secure($method, $path, $body)
            ->then(
                fn(string $serialized): array => $this->deserializer->deserializeCollection($serialized, $className)
            );
    }

    /**
     * @return PromiseInterface<string>
     */
    public function secure(string $method, string $path, ?array $body = null): PromiseInterface
    {
        $payload = json_encode($body);
        $this->logger->info("Executing secure request {$method} {$path} {$payload}", ['body' => $body]);

        $executeRequest =
            /** @return PromiseInterface<ResponseInterface> */
            fn(): PromiseInterface => $this->doSecure($method, $path, $body);

        /** @var PromiseInterface<string> */
        return $executeRequest()
            ->then(function (ResponseInterface $response) use ($executeRequest): PromiseInterface {
                return
                    $this
                        ->checkResponse($response, $executeRequest);
            })
            ->catch(function (\Throwable $e): void {
                $this->logger->error($e->getMessage());
            });
    }

    /**
     * @return PromiseInterface<ResponseInterface>
     */
    private function doSecure(string $method, string $path, ?array $body): PromiseInterface
    {
        return $this
            ->session
            ->get()
            ->then(function (string $session) use ($method, $path, $body): PromiseInterface {
                return $this
                    ->browser
                    ->request($method, $path, ['X-Fbx-App-Auth' => $session], $body === null ? '' : json_encode($body));
            });
    }

    /**
     * @return PromiseInterface<string>
     */
    private function checkResponse(ResponseInterface $response, ?\Closure $retry): PromiseInterface
    {
        if ($response->getStatusCode() === 403) {
            if ($retry === null) {
                throw new \RuntimeException('Unable to connect to freebox API');
            }

            /** @var PromiseInterface<string> */
            return $this
                ->session
                ->renew()
                ->then($retry(...))
                ->then(
                    fn(ResponseInterface $response): PromiseInterface => $this->checkResponse($response, null)
                );
        }

        $this->logger->debug("HTTP Response {$response->getStatusCode()}");

        $body = (string) $response->getBody();

        return resolve($body);
    }
}
