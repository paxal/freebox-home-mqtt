<?php

declare(strict_types=1);

namespace Paxal\FreeboxHomeMqtt\Freebox;

use Paxal\FreeboxHomeMqtt\Freebox\DTO\Adapter\AdapterResponse;
use Paxal\FreeboxHomeMqtt\Freebox\DTO\Node\NodeResponse;
use Paxal\FreeboxHomeMqtt\Freebox\DTO\Tile\TileDataResponse;
use Paxal\FreeboxHomeMqtt\Freebox\DTO\Tile\TileResponse;
use Paxal\FreeboxHomeMqtt\Freebox\Object\Adapter;
use Paxal\FreeboxHomeMqtt\Freebox\Object\Tile;
use Paxal\FreeboxHomeMqtt\Freebox\Object\TileData;
use Paxal\FreeboxHomeMqtt\Freebox\ObjectFactory\AdapterFactory;
use Paxal\FreeboxHomeMqtt\Freebox\ObjectFactory\NodeFactory;
use Paxal\FreeboxHomeMqtt\Freebox\ObjectFactory\TileFactory;
use Paxal\FreeboxHomeMqtt\Freebox\ObjectRepository\TileRepository;
use Psr\EventDispatcher\EventDispatcherInterface;
use Psr\Log\LoggerInterface;
use React\Promise\PromiseInterface;

final class NodeUpdater
{
    private const PATH_PREFIX = '/api/v8/home/';
    private const PATH_ADAPTERS = self::PATH_PREFIX . 'adapters';
    private const PATH_NODES = self::PATH_PREFIX . 'nodes';
    private const PATH_TILESET_ALL = self::PATH_PREFIX . 'tileset/all';

    public function __construct(
        private readonly ApiClient $apiClient,
        private readonly AdapterFactory $adapterFactory,
        private readonly NodeFactory $nodeFactory,
        private readonly TileFactory $tileFactory,
        private readonly EventDispatcherInterface $eventDispatcher,
        private readonly TileRepository $tileRepository,
        private readonly LoggerInterface $logger,
    ) {}

    public function first(): PromiseInterface
    {
        $adapters = $this
            ->apiClient
            ->secureObjectCollection(AdapterResponse::class, 'GET', self::PATH_ADAPTERS)
            ->then(
                /**
                 * @param AdapterResponse[] $adapterResponses
                 *
                 * @return Adapter[]
                 */
                fn(array $adapterResponses): array => array_map(
                    $this->adapterFactory->handleResponse(...),
                    $adapterResponses
                )
            );

        $nodes = $adapters
            ->then(
                fn(): PromiseInterface => $this
                    ->apiClient
                    ->secureObjectCollection(NodeResponse::class, 'GET', self::PATH_NODES)
            )
            ->then(
                fn(array $nodeResponses): array => array_map(
                    $this->nodeFactory->handleResponse(...),
                    $nodeResponses
                )
            );


        return $nodes->then($this->update(...));
    }

    public function update(): PromiseInterface
    {
        $tileResponses = $this
                    ->apiClient
                    ->secureObjectCollection(TileResponse::class, 'GET', self::PATH_TILESET_ALL);

        $tiles = $tileResponses
            ->then(
                fn(array $tileResponses): array => array_map(
                    $this->tileFactory->handleResponse(...),
                    $tileResponses
                )
            );

        return $tiles->then(
            fn(): PromiseInterface => $tileResponses->then($this->updateTiles(...))
        );
    }

    /**
     * @param list<TileResponse> $responses
     */
    private function updateTiles(array $responses): void
    {
        foreach ($responses as $response) {
            $tile = $this->tileRepository->get(Tile::hash(nodeId: $response->nodeId, type: $response->type, action: $response->action));
            if ($tile === null) {
                $this->logger->error('Unable to find corresponding tile');
                continue;
            }

            foreach ($response->data as $dataResponse) {
                // Find corresponding $data
                $tileData = $this->findDataFromResponse($dataResponse, $tile->data);
                if ($tileData === null) {
                    $this->logger->error('Unable to find corresponding tile data');
                    continue;
                }

                if ($tileData->value === $dataResponse->value) {
                    continue;
                }
                $tileData->value = $dataResponse->value;

                $this->eventDispatcher->dispatch(new FreeboxUpdatedEvent($tileData));
            }
        }
    }

    /**
     * @param list<TileData> $tileDataCollection
     */
    private function findDataFromResponse(TileDataResponse $dataResponse, array $tileDataCollection): ?TileData
    {
        foreach ($tileDataCollection as $tileData) {
            if ($tileData->epId === $dataResponse->epId) {
                return $tileData;
            }
        }

        return null;
    }
}
