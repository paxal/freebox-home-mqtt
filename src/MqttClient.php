<?php

declare(strict_types=1);

namespace Paxal\FreeboxHomeMqtt;

use PhpMqtt\Client\ConnectionSettings;
use PhpMqtt\Client\MqttClient as BaseMqttClient;
use React\EventLoop\LoopInterface;
use React\EventLoop\TimerInterface;

/**
 * @psalm-suppress PropertyNotSetInConstructor
 */
class MqttClient extends BaseMqttClient
{
    public ConnectionSettings $settings;

    /** @var resource|null */
    private $onReadStreamSocket = null;

    private TimerInterface|null $timer = null;

    public function connectWithSettings(): void
    {
        $this->connect($this->settings);
    }

    public function registerEventLoopReadStream(LoopInterface $loop): void
    {
        if ($this->timer !== null || $this->onReadStreamSocket !== null) {
            throw new \LogicException(
                '[mqtt-bridge] Should not happen: timer or socket not null, should have been previously cleared'
            );
        }

        if ($this->socket === null) {
            throw new \LogicException('[mqtt-bridge] Do not ask for socket unless MqttClient has started');
        }

        // Add timer for HeartBeat, backup for unregister
        $this->timer = $loop->addPeriodicTimer(
            (float) $this->settings->getKeepAliveInterval() / 2,
            $this->oneLoop(...)
        );

        // Add read stream for 0-CPU interactive computing.
        // Backup in case the MQTT class will invalidate it on failure.
        $this->onReadStreamSocket = $this->socket;

        $loop->addReadStream(
            $this->onReadStreamSocket,
            $this->oneLoop(...)
        );

        // Run loop once
        $this->oneLoop();
    }

    public function unregisterEventLoopReadStream(LoopInterface $loop): void
    {
        if ($this->timer !== null) {
            $loop->cancelTimer($this->timer);
            $this->timer = null;
        }

        if ($this->onReadStreamSocket === null) {
            return;
        }

        $loop->removeReadStream($this->onReadStreamSocket);
        $this->onReadStreamSocket = null;
    }

    private function oneLoop(): void
    {
        $this->loopOnce(microtime(true), allowSleep: false);
    }
}
