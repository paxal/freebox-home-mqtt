<?php

declare(strict_types=1);

namespace Paxal\FreeboxHomeMqtt;

use PhpMqtt\Client\ConnectionSettings;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;

final class MqttClientFactory
{
    public function __construct(
        private readonly DenormalizerInterface $denormalizer,
        private readonly LoggerChain $logger,
    ) {}

    /**
     * @param \Traversable<mixed> $config
     */
    public function create(\Traversable $config): MqttClient
    {
        $config = (array) $config;
        $connectionSettings = $config['connection_settings'] ?? null;
        if (!\is_array($connectionSettings) && $connectionSettings !== null) {
            throw new \InvalidArgumentException('Invalid connectionSettings: should be an array or null');
        }
        unset($config['connection_settings']);
        $config['logger'] = $this->logger;

        $client = $this->denormalizer->denormalize($config, MqttClient::class);
        \assert($client instanceof MqttClient);

        $connectionSettingsObject = $this->denormalizeSettings($connectionSettings ?? []);
        $client->settings = $connectionSettingsObject;

        return $client;
    }

    /**
     * @param array<mixed> $array
     */
    private function denormalizeSettings(array $array): ConnectionSettings
    {
        $settings = new ConnectionSettings();
        /** @var mixed $value */
        foreach ($array as $key => $value) {
            \assert(\is_string($key));
            $setter = 'set' . ucfirst($key);
            if (!\is_callable([$settings, $setter])) {
                throw new \InvalidArgumentException('Invalid connection settings property: ' . $key);
            }
            /** @psalm-suppress MixedAssignment */
            $settings = $settings->$setter($value);
            \assert($settings instanceof ConnectionSettings);
        }

        return $settings;
    }
}
