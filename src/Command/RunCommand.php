<?php

declare(strict_types=1);

namespace Paxal\FreeboxHomeMqtt\Command;

use Paxal\FreeboxHomeMqtt\Freebox\NodeUpdater;
use Paxal\FreeboxHomeMqtt\MqttClient;
use Paxal\FreeboxHomeMqtt\MqttSubscriptionEvent;
use Paxal\FreeboxHomeMqtt\ResetEvent;
use Psr\EventDispatcher\EventDispatcherInterface;
use Psr\Log\LoggerInterface;
use React\EventLoop\LoopInterface;
use React\EventLoop\TimerInterface;
use React\Promise\PromiseInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

final class RunCommand extends Command
{
    private bool $subscriptionIsOn = false;
    private ?TimerInterface $freeboxTimer = null;

    public function __construct(
        private readonly LoopInterface $loop,
        private readonly MqttClient $mqttClient,
        private readonly LoggerInterface $logger,
        private readonly EventDispatcherInterface $eventDispatcher,
        private readonly NodeUpdater $nodeUpdater,
    ) {
        parent::__construct();
    }

    public static function getDefaultName(): string
    {
        return 'app:run';
    }

    public static function getDefaultDescription(): string
    {
        return 'Run daemon. You first need to authenticate.';
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->mqttClient->registerConnectedEventHandler(
            function (): void {
                $this->nodeUpdater->first()->then($this->runLoop(...))->then(
                    fn() => $this->loop->addTimer(1, fn() => $this->subscriptionIsOn = true)
                );
                $this->mqttClient->subscribe(
                    'freebox/#',
                    function (string $topic, string $message, bool $retained): void {
                        if (!$this->subscriptionIsOn) {
                            return;
                        }

                        $this->eventDispatcher->dispatch(new MqttSubscriptionEvent($topic, $message, $retained));
                    }
                );
            }
        );

        $restart = true;

        if (\extension_loaded('pcntl')) {
            /** @psalm-suppress UnusedPsalmSuppress,UndefinedConstant,MixedArgument */
            $this->loop->addSignal(SIGINT, $self = function () use (&$restart, &$self): void {
                $restart = false;
                $this->loop->stop();
                $this->logger->info('[run] Caught SIGINT, stopping...');
                \assert(\extension_loaded('pcntl'));
                /** @psalm-suppress UnusedPsalmSuppress,UndefinedConstant,MixedArgument */
                $this->loop->removeSignal(SIGINT, $self);
            });
        }

        while ($restart) {
            try {
                $this->mqttClient->connectWithSettings();
                $this->mqttClient->registerEventLoopReadStream($this->loop);
                $this->loop->run();
                $restart = false;
            } catch (\Throwable $e) {
                $this->logger->error("[run] " . $e::class . " " . $e->getMessage());
                // Avoid 100% CPU on continuous failure...
                sleep(1);
            } finally {
                // Reset subscription
                $this->subscriptionIsOn = false;
                // Unregister read stream
                $this->mqttClient->unregisterEventLoopReadStream($this->loop);
                // Kill freebox timer if any
                if ($this->freeboxTimer !== null) {
                    $this->loop->cancelTimer($this->freeboxTimer);
                }
                $this->freeboxTimer = null;

                $this->eventDispatcher->dispatch(new ResetEvent());
            }
        }

        return 0;
    }

    private function runLoop(): void
    {
        $this->freeboxTimer = $this->loop->addTimer(
            2,
            fn(): PromiseInterface => $this
                ->nodeUpdater
                ->update()
                ->then($this->runLoop(...))
        );
    }
}
