<?php

declare(strict_types=1);

namespace Paxal\FreeboxHomeMqtt\Command;

use Paxal\FreeboxHomeMqtt\Freebox\Authenticator;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

final class AuthCommand extends Command
{
    public function __construct(
        private readonly Authenticator $authenticator,
    ) {
        parent::__construct();
    }

    public static function getDefaultName(): string
    {
        return 'app:auth';
    }

    public static function getDefaultDescription(): string
    {
        return 'Run once to authenticate the application on your Freebox';
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this
            ->authenticator
            ->authenticate($output->writeln(...))
            ->then(static function (string $appToken) use ($output): void {
                $output->writeln([
                    'Please save app_token in config.yaml, then go to http//mafreebox.freebox.fr to allow app access to connected objects',
                    $appToken,
                ]);
            });

        return 0;
    }
}
