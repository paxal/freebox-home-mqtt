<?php

declare(strict_types=1);

namespace Paxal\FreeboxHomeMqtt\Command;

use Paxal\FreeboxHomeMqtt\Freebox\ApiClient;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

use function Safe\json_decode as json_decode;

final class ApiCommand extends Command
{
    public function __construct(private readonly ApiClient $apiClient)
    {
        parent::__construct();
    }

    public static function getDefaultName(): string
    {
        return 'debug:api';
    }

    public static function getDefaultDescription(): string
    {
        return 'Test Freebox API';
    }

    protected function configure(): void
    {
        $this
            ->addArgument('method', InputArgument::REQUIRED, 'Method: GET, POST, ...')
            ->addArgument('path', InputArgument::REQUIRED, 'Relative path, eg /api/v8/home/nodes')
            ->addArgument('payload', InputArgument::OPTIONAL, 'Payload to send, as json object/array');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $method = (string) $input->getArgument('method');
        $path = (string) $input->getArgument('path');
        $payload = $input->getArgument('payload');
        \assert($payload === null || \is_string($payload));
        if ($payload !== null) {
            /** @var mixed $payload */
            $payload = json_decode($payload, true);
            if (!\is_array($payload)) {
                throw new \InvalidArgumentException('Invalid JSON payload: not an array');
            }
        }

        $this
            ->apiClient
            ->secure($method, $path, $payload)
            ->then(static function (string $response) use ($output): void {
                $output->writeln($response);
            });

        return 0;
    }
}
