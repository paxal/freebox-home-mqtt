<?php

declare(strict_types=1);

namespace Paxal\FreeboxHomeMqtt\Topic;

use React\Promise\PromiseInterface;

final class SlotTopic extends Topic
{
    /**
     * @param string $name
     * @param (\Closure(string):PromiseInterface)|null $setter
     */
    public function __construct(
        string $name,
        ?\Closure $setter = null
    ) {
        parent::__construct(
            $name,
            getter: static fn(): string => 'null',
            setter: $setter
        );
    }
}
