<?php

declare(strict_types=1);

namespace Paxal\FreeboxHomeMqtt\Topic;

use Paxal\FreeboxHomeMqtt\Freebox\Object\TileData;

final class SignalTopic extends Topic
{
    /** @param \Closure():string $getter */
    public function __construct(
        string $name,
        public TileData $data,
        \Closure $getter,
    ) {
        parent::__construct(
            name: $name,
            getter: $getter,
        );
    }
}
