<?php

declare(strict_types=1);

namespace Paxal\FreeboxHomeMqtt\Topic;

use React\Promise\PromiseInterface;

abstract class Topic
{
    public function __construct(
        public readonly string $name,
        /** @var \Closure():string */
        private readonly \Closure $getter,
        /** @var (\Closure(string):?PromiseInterface)|null */
        private readonly ?\Closure $setter = null,
    ) {}

    public function __invoke(string $payload): ?PromiseInterface
    {
        if ($this->setter === null) {
            return null;
        }

        return ($this->setter)($payload);
    }

    public function get(): string
    {
        return ($this->getter)();
    }
}
