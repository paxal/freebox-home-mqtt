<?php

declare(strict_types=1);

namespace Paxal\FreeboxHomeMqtt\Topic;

use Paxal\FreeboxHomeMqtt\Freebox\Object\Tile;
use Paxal\FreeboxHomeMqtt\Freebox\Object\TileData;
use Paxal\FreeboxHomeMqtt\Freebox\ValueUpdater;
use Paxal\FreeboxHomeMqtt\ValueNormalizer;
use Psr\Log\LoggerInterface;
use React\EventLoop\LoopInterface;
use React\Promise\Deferred;
use React\Promise\PromiseInterface;

final class FreeboxValueTopicFactory
{
    /** Avoid call overlaps because it messes with Home-IO protocol */
    private const LOCK_TIMER = 2;
    private const LOOP_INTERVAL = .2;

    /** @var array<(\Closure():PromiseInterface)> */
    private array $invokes;
    private bool $locked;

    public function __construct(
        private readonly ValueUpdater $valueUpdater,
        private readonly ValueNormalizer $valueNormalizer,
        private readonly LoggerInterface $logger,
        private readonly LoopInterface $loop,
    ) {
        $this->invokes = [];
        $this->locked = false;
        $this->loop->addPeriodicTimer(self::LOOP_INTERVAL, $this->loop(...));
    }

    private function loop(): void
    {
        if ($this->locked) {
            return;
        }

        if (\count($this->invokes) === 0) {
            return;
        }

        $this->locked = true;

        /* @var \Closure():PromiseInterface $invoke */
        $invoke = array_shift($this->invokes);
        $invoke()
            ->finally(
                function (): void {
                    $this
                        ->loop
                        ->addTimer(self::LOCK_TIMER, function (): void {
                            $this->locked = false;
                        });
                }
            );
    }

    public function createSignalTopic(Tile $tile, TileData $tileData): SignalTopic
    {
        return new SignalTopic(
            name: $this->createTopicName($tile, $tileData, 'value'),
            data: $tileData,
            getter: fn(): string => $this->valueNormalizer->normalize($tileData->value),
        );
    }

    public function createSlotTopic(Tile $tile, TileData $tileData): SlotTopic
    {
        return new SlotTopic(
            name: $this->createTopicName($tile, $tileData, 'set'),
            setter: $this->createInvoke($tileData),
        );
    }

    private function createTopicName(Tile $tile, TileData $tileData, string $property): string
    {
        return sprintf("freebox/home/%s/%s/%s", $tile->id, $tileData->name, $property);
    }

    /**
     * @param TileData $tileData
     *
     * @return \Closure(string):PromiseInterface
     */
    private function createInvoke(TileData $tileData): \Closure
    {
        return function (string $payload) use ($tileData): PromiseInterface {
            $deferred = new Deferred();
            $this->invokes[] = fn(): PromiseInterface => $this
                ->valueUpdater
                ->update($tileData, $payload)
                ->then(function (): void {
                    $this->logger->info('Value updated on Freebox');
                })
                ->then(static fn() => $deferred->resolve(null))
                ->catch(static fn(\Throwable $e) => $deferred->reject($e));

            $this->loop();

            return $deferred->promise();
        };
    }
}
