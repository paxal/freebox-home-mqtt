<?php

declare(strict_types=1);

namespace Paxal\FreeboxHomeMqtt\Topic;

use Paxal\FreeboxHomeMqtt\Freebox\Object\TileData;
use Paxal\FreeboxHomeMqtt\MqttSubscriptionEvent;
use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

final class TopicRepository implements EventSubscriberInterface
{
    /** @var array<string, Topic> */
    private array $topicsByName;

    /** @var \SplObjectStorage<TileData, Topic> */
    private \SplObjectStorage $topicsByData;

    public function __construct(private readonly LoggerInterface $logger)
    {
        $this->topicsByName = [];
        $this->topicsByData = new \SplObjectStorage();
    }

    public function add(Topic $topic): void
    {
        $this->topicsByName[$topic->name] = $topic;
        if (!($topic instanceof SignalTopic)) {
            return;
        }

        $this->topicsByData[$topic->data] = $topic;
    }

    public function getByData(TileData $tileData): ?Topic
    {
        return $this->topicsByData[$tileData] ?? null;
    }

    public function onEvent(MqttSubscriptionEvent $event): void
    {
        if ($event->retained) {
            return;
        }

        ($this->topicsByName[$event->topic] ?? null)
            ?->__invoke($event->payload)
            ?->then(fn() => $this->logger->info('Value updated successfully topic:{topic}', ['topic' => $event->topic]))
            ?->catch(fn() => $this->logger->error('Unable to update value topic:{topic}', ['topic' => $event->topic]));
    }

    public static function getSubscribedEvents(): array
    {
        return [MqttSubscriptionEvent::class => 'onEvent'];
    }

    public function get(string $topic): ?Topic
    {
        return $this->topicsByName[$topic] ?? null;
    }
}
