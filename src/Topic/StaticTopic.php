<?php

declare(strict_types=1);

namespace Paxal\FreeboxHomeMqtt\Topic;

final class StaticTopic extends Topic
{
    public function __construct(string $topic, string $payload)
    {
        parent::__construct($topic, getter: static fn(): string => $payload);
    }
}
