<?php

declare(strict_types=1);

namespace Paxal\FreeboxHomeMqtt;

use Symfony\Component\Console\Application;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Logger\ConsoleLogger;
use Symfony\Component\Console\Output\OutputInterface;

final class App extends Application
{
    public function __construct(
        private LoggerChain $loggerChain,
    ) {
        parent::__construct();
    }

    public function doRun(InputInterface $input, OutputInterface $output): int
    {
        $this->loggerChain->add(new ConsoleLogger($output));

        return parent::doRun($input, $output);
    }
}
