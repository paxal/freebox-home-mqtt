<?php

declare(strict_types=1);

namespace Paxal\FreeboxHomeMqtt;

use Paxal\FreeboxHomeMqtt\Freebox\Object\ValueType;

final class ValueNormalizer
{
    public function normalize(string|int|bool|float|null $value): string
    {
        return match (true) {
            \is_bool($value) => \Safe\json_encode($value),
            \is_string($value) => $value,
            \is_int($value), \is_float($value), $value === null => (string) $value,
        };
    }

    public function denormalize(ValueType $valueType, string $value): string|int|bool|float|null
    {
        // Only case where we allow to return null, otherwise we should fail
        if ($valueType === ValueType::VOID) {
            return null;
        }

        return match ($valueType) {
            ValueType::BOOL => filter_var($value, FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE),
            ValueType::FLOAT => filter_var($value, FILTER_VALIDATE_FLOAT, FILTER_NULL_ON_FAILURE),
            ValueType::INT => filter_var($value, FILTER_VALIDATE_INT, FILTER_NULL_ON_FAILURE),
            ValueType::STRING => $value,
        } ?? throw new \LogicException("Unable to parse data for valueType:{$valueType->value} and json value:" . \Safe\json_encode($value));
    }
}
