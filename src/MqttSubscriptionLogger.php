<?php

declare(strict_types=1);

namespace Paxal\FreeboxHomeMqtt;

use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

final class MqttSubscriptionLogger implements EventSubscriberInterface
{
    public function __construct(private readonly LoggerInterface $logger) {}

    public static function getSubscribedEvents(): array
    {
        return [MqttSubscriptionEvent::class => 'onSubscriptionEvent'];
    }

    public function onSubscriptionEvent(MqttSubscriptionEvent $event): void
    {
        $this->logger->info('Received published event topic:{topic} payload:{payload}', [
            'topic' => $event->topic,
            'payload' => $event->payload,
        ]);
    }
}
