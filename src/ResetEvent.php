<?php

declare(strict_types=1);

namespace Paxal\FreeboxHomeMqtt;

use Symfony\Contracts\EventDispatcher\Event;

final class ResetEvent extends Event {}
