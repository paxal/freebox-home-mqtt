<?php

declare(strict_types=1);

namespace Paxal\FreeboxHomeMqtt;

use Symfony\Contracts\EventDispatcher\Event;

final class MqttSubscriptionEvent extends Event
{
    public function __construct(
        public readonly string $topic,
        public readonly string $payload,
        public readonly bool $retained,
    ) {}
}
