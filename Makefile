PHP ?= php8.3

test_in_docker:
	make test PHP="docker compose exec php"

test: php_cs_fixer psalm phpcs phpunit

php_cs_fixer:
	$(PHP) ./vendor/bin/php-cs-fixer fix

psalm:
	$(PHP) ./vendor/bin/psalm --no-cache

phpcs phpcbf:
	$(PHP) ./vendor/bin/$@ src bin

precommit: php_cs_fixer psalm phpcbf

phpunit:
	$(PHP) ./vendor/bin/phpunit tests/
